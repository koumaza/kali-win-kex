#!/bin/bash

red='\033[1;31m'
green='\033[1;32m'
yellow='\033[1;33m'
blue='\033[1;34m'
light_cyan='\033[1;96m'
reset='\033[0m'

XSTARTUP="/usr/lib/win-kex/xstartup"
WINKEX="/usr/lib/win-kex/win-kex"

function help() {
    printf "\n\tWin-Kex is a client server environment based on Tigervnc"
    printf "\n\tthat adds a GUI desktop to Kali in Windows Subsystem for Linux 2 (WSL 2)"
    printf "\n\n\tUsage:"
    printf "\n\t\tkex\t\t : start kex server and launch win-kex client"
    printf "\n\t\tkex wtstart\t : start kex server and launch win-kex client"
    printf "\n\t\t\t\t   in Windows Terminal session"
    printf "\n\t\tkex start\t : start kex server"
    printf "\n\t\tkex status\t : show kex server status"
    printf "\n\t\tkex start-client : start kex client"
    printf "\n\t\tkex stop\t : stop kex server"
    printf "\n\t\tkex kill\t : stop kex server and kill all related processes"
    printf "\n\t\tkex passwd\t : set kex server password"
    printf "\n\n"
}
function ask() {
    # http://djm.me/ask
    while true; do

        if [ "${2:-}" = "Y" ]; then
            prompt="Y/n"
            default=Y
        elif [ "${2:-}" = "N" ]; then
            prompt="y/N"
            default=N
        else
            prompt="y/n"
            default=
        fi

        # Ask the question
        printf "${light_cyan}$1"
        read -p " [$prompt] " REPLY

        # Default?
        if [ -z "$REPLY" ]; then
            REPLY=$default
        fi

        printf "${reset}"

        # Check if the reply is valid
        case "$REPLY" in
            Y*|y*) return 0 ;;
            N*|n*) return 1 ;;
        esac
    done
}

function start-kex() {
    if [ ! -f ~/.vnc/passwd ]; then
        passwd-kex
    fi
    USR=$(whoami)
    if [ $USR == "root" ]; then
        SCREEN=":2"
    else
        SCREEN=":1"
    fi 
    vncserver -useold -xstartup ${XSTARTUP} -SecurityTypes=VeNCrypt,TLSVnc $SCREEN >/dev/null 2>&1 </dev/null
    starting_kex=1
    return 0
}

function stop-kex() {
    vncserver -kill :1 | sed s/"Xtigervnc"/"Win-KeX"/
    vncserver -kill :2 | sed s/"Xtigervnc"/"Win-KeX"/
    return $?
}

function passwd-kex() {
    vncpasswd
    return $?
}

function status-kex() {
    sessions=$(vncserver -list | sed s/"TigerVNC"/"Win-KeX"/)
    if [[ $sessions == *"590"* ]]; then
        printf "\n${sessions}\n"
        printf "\nYou can use the Win-KeX client to connect to any of these displays.\n\n"
    else
        if [ ! -z \$starting_kex ]; then
            printf '\nError connecting to the KeX server.\n'
            printf 'Please try "kex start" to start the service.\n'
            printf 'If the server fails to start, please try "kex kill" or restart your WSL2 session and try again.\n\n'
        fi
    fi
    return 0
}

function start-client() {
    ${WINKEX} -SecurityTypes VeNCrypt,TLSVnc -passwd $HOME/.vnc/passwd FullScreen=1 127.0.0.1:1 &
}

function start-wtclient() {
    while true
    do
        clear
        printf "\n\t${blue}Win-KeX session is active\n\tClose this window to terminate Win-Kex\n${reset}"
        ${WINKEX} -SecurityTypes VeNCrypt,TLSVnc -passwd $HOME/.vnc/passwd FullScreen=1 127.0.0.1:1
        if ask "\n\tWin-Kex session disconnected.\n\tWould you like to reconnect?" "Y"; then
            printf "\n\tReconnecting Win-Kex session\n"
        else
            stop-kex
            exit 0
        fi
    done
}

function kill-kex() {
    pkill Xtigervnc
    pkill xiccd
    return $?
}

case $1 in
    start)
        start-kex
        ;;
    start-client)
        start-client
        ;;
    wtstart)
        start-kex
        start-wtclient
        ;;
    stop)
        stop-kex
        ;;
    status)
        status-kex
        ;;
    passwd)
        passwd-kex
        ;;
    kill)
        kill-kex
        ;;
    help|-h|--help)
        help
        ;;
    *)
        start-kex
        status-kex
        start-client
        ;;
esac

